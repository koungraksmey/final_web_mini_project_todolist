import React from 'react'
import { Link, NavLink, useNavigate } from 'react-router-dom'
import { getTokenString } from '../utils/ProvideToken'

const WelcomePage = () => {
  const token = getTokenString()
  const navigate = useNavigate()

  return (
    <>
      <header class=" bg-[#4ECDC4] fixed top-0 right-0 left-0">
        <nav class="flex justify-between items-center py-4 px-6">
          <button className="gradient-bg relative left-0 text-white rounded-xl px-2 py-1">
            <h1 class=" font-bold text-xl ">Task Zone</h1>
          </button>
          <div>
            {/* <a
              href="#"
              class="bg-white text-indigo-500 py-2 px-4 rounded-full mr-4 hover:bg-indigo-500 hover:text-white transition duration-300 ease-in-out"
            >
              Login
            </a> */}
            <Link
              to={token == null ? '/login' : '/home/board'}
              class="bg-white text-indigo-500 py-2 px-4 rounded-full hover:bg-indigo-500 hover:text-white transition duration-300 ease-in-out"
            >
              Home
            </Link>
          </div>
        </nav>
      </header>
      <div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100">
        <div class="text-center">
          <h1 class="text-4xl font-medium text-gray-900">Task Zone</h1>
        </div>

        <div class="flex flex-wrap justify-center mt-10">
          <div class="w-full sm:w-1/2 p-6">
            <div class="bg-image h-full"></div>
          </div>

          <div class="w-full sm:w-1/2 p-6">
            <h2 class="text-2xl font-medium text-gray-900 mb-4">
              The Fastest Way to Complete Your Tasks with Web Apps
            </h2>
            <p class="text-gray-600 mb-4">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris
              fringilla velit elit, eget suscipit enim facilisis sit amet. Sed
              nec tellus nisl. Nam eu justo in massa.
            </p>
            <div class="flex justify-center">
              {token == null ? (
                <>
                  <Link
                    to="/login"
                    class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mr-4"
                  >
                    Log In
                  </Link>
                  <Link
                    to="/signup"
                    class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded"
                  >
                    Sign Up
                  </Link>
                </>
              ) : (
                <div>
                  <h1 class="pt-9 text-4xl font-medium text-gray-900">
                    Welcome to Task Zone Apps
                  </h1>
                  <em className="block">You are login in</em>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>

      <footer class="bg-[#4ECDC4] py-4">
        <div class="container mx-auto text-center text-white font-bold text-sm">
          <p>&copy; 2023 Task Zone. All rights reserved.</p>
        </div>
      </footer>
    </>
  )
}

export default WelcomePage
