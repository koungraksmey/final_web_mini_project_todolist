import React from "react";
import { Outlet } from "react-router-dom";
import SideBar from "./sideBar/SideBar";

const Root = () => {
  return (
    <div>
      <SideBar />
      <Outlet />
    </div>
  );
};

export default Root;
