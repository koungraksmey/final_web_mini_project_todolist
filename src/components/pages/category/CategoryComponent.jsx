import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchCategory } from "../../services/CategoryService";
import moment from "moment";
import { Button, Modal } from "flowbite-react";
import CategoryAddPopUp from "./CategoryAddPopUp";
import CategoryUpdatePopUp from "./CategoryUpdatePopUp";
import CategoryDeletePopUp from "./CategoryDeletePopUp";
import { Link } from "react-router-dom";

const CategoryComponent = () => {
  const [isShow, setIsShow] = useState();

  const [itemUpdate, setItemUpdate] = useState({});
  const getItem = (item) => {
    setItemUpdate(item);
  };

  const [idDelete, setIdDelete] = useState();
  const getId = (id) => {
    setIdDelete(id);
  };

  const dispatch = useDispatch();

  const data = useSelector((state) => state.category.categories);

  useEffect(() => {
    dispatch(fetchCategory());
  }, [dispatch]);
  // console.log("data",data);

  return (
    <div class=" pl-44 pr-44   pt-4 sm:ml-64">
      <h2 className="mb-2 pt-8 pb-8 text-5xl tracking-tight text-gray-900 dark:text-white">
        Categories
      </h2>

      <div className="grid grid-cols-2 md:grid-cols-3 gap-4">
        <div className=" w-12/12 h-40 text-center max-w-sm p-6 mb-8 ml-8 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
          <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
            Create Category
          </h5>
          <CategoryAddPopUp />
        </div>

        {data?.map((item) => (
          <div
            key={item.categoryId}
            className="w-12/12 h-40  text-left max-w-sm p-8 pl-12 mb-8 ml-8 bg-white border  rounded-lg shadow select-none border-l-4 border-blue-400  font-medium hover:border-blue-500"
          >
            <div className="flex justify-between">
              
            <Link to="/home/taskByCategory"
             state={item}
             >
              <h4 className="mb-2  text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                {item?.categoryName}
              </h4>

              <p className="pt-6  text-1xl tracking-tight text-gray-700 dark:text-white">
                Created : {moment(item?.timestamp).format("ddd,MMM DD")}
              </p>
            </Link>
            <div className="dropdown dropdown-right align-middle ">
                <label tabIndex={0} className=" m-1 text-2xl text-gray-500  ">
                  <button>...</button>
                </label>

                <ul
                  tabIndex={0}
                  className="dropdown-content menu p-2 shadow bg-base-100 rounded-box w-52"
                >
                  <li onClick={() => getItem(item)}>
                    <CategoryUpdatePopUp itemUpdate={itemUpdate} />
                    {/* <CategoryDaisyPopUp/> */}
                  </li>
                  <li onClick={() => getId(item.categoryId)}>
                    <CategoryDeletePopUp id={idDelete} />
                  </li>
                </ul>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default CategoryComponent;
