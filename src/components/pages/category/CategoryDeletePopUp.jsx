import { Button, Label, Modal } from 'flowbite-react'
import React, { useState } from 'react'
import { HiOutlineExclamationCircle } from "react-icons/hi";
import { useDispatch } from 'react-redux';
import { deleteCategory } from '../../services/CategoryService';

const CategoryDeletePopUp = (data) => {
    // console.log(data)
    const [show,setShow]=useState(false)

    const dispatch = useDispatch();
    const updateCategoryHandler = (e) => {
       
        e.preventDefault();
        const body = {
     
          idDelete:data.id,
          id:data.id
        };
        dispatch(deleteCategory(body));
        setShow(false)
      };
  return (
    <div>
        <React.Fragment>
  <Label onClick={()=>setShow(true)} className='text-red-600'>
    Delete
  </Label>
  <Modal
    show={show}
    size="sm"
    popup={true}
    onClick={()=>setShow(false)}
  >
    <Modal.Header />
    <Modal.Body>
      <div className="text-center ">
        <HiOutlineExclamationCircle className="mx-auto mb-4 h-14 w-14 text-gray-400 dark:text-gray-200" />
        <h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
          Are you sure you want to delete this product?
        </h3>
        <div className="flex justify-center gap-4">
          <Button
            className='bg-red-500 hover:bg-red-700'
            onClick={updateCategoryHandler}
          >
            Yes, I'm sure
          </Button>
          <Button
            color="gray"
            onClick={()=>setShow(false)}
          >
            No, cancel
          </Button>
        </div>
      </div>
    </Modal.Body>
  </Modal>
</React.Fragment>
    </div>
  )
}

export default CategoryDeletePopUp