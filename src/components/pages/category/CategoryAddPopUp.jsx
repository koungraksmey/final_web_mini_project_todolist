import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { createCategory } from "../../services/CategoryService";
import { Button, Label, Modal, TextInput } from "flowbite-react";

const CategoryAddPopUp = () => {
  const dispatch = useDispatch();

  const [show,setShow]=useState(false)
  // category
  //create category
  const [newData, setNewData] = useState({});

  // const [categoryName,setCategoryName] = useState()
  const addNewCategory = (e) => {
    e.preventDefault();
    const body = {
      categoryName: newData,
    };
    dispatch(createCategory(body));
    setShow(false)
  };

  return (
    <React.Fragment>
  <Label onClick={()=>setShow(true)}>
  <button
        htmlFor="my-modal-3"
        className="btn mt-4 bg-blue-600 rounded-full w-12 h-12"
      >
        <svg
          className="w-5 h-5 text-white "
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
          aria-hidden="true"
        >
          <path
            clip-rule="evenodd"
            fill-rule="evenodd"
            d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z"
          ></path>
        </svg>
      </button>
    
  </Label>
  <Modal
    show={show}
    size="sm"
    popup={true}
    onClose={()=>setShow(false)}
  >
   
   <Modal.Header className="p-6">
     Create Category
    </Modal.Header>
    <Modal.Body>
      <div className="space-y-6 px-6 pb-4 sm:pb-6 lg:px-8 xl:pb-8">
       
        <div>
          <div className="mb-2 block">
            <Label
              htmlFor=""
              value="Category Name"
            />
          </div>
          <TextInput
            id=""
            onBlur={(e)=>setNewData(e.target.value)}
            placeholder="category name"
            required={true}
          />
        </div>
       
        <div className="w-full">
          <Button onClick={addNewCategory}  type='submit' className="w-full">
            Create
          </Button>
        </div>
       
      </div>
    </Modal.Body>
  </Modal>
</React.Fragment>
  );
};

export default CategoryAddPopUp;
