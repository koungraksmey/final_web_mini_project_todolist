import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { getAllTaskByCategory } from '../../services/CategoryService';
import { useLocation } from 'react-router';
import moment from 'moment';
import { DELECT_TASK_BYID } from '../../services/TaskService';
import { deleteTaskById } from '../../redux/features/Tasks/taskSlice';

const CategoryView = (props) => {
    const location = useLocation();
    const propsData = location.state;
  console.log(propsData.categoryId);
    const dispatch = useDispatch();
  
  const data = useSelector((state) => state.category.categories);

  useEffect(() => {
    dispatch(getAllTaskByCategory(propsData.categoryId));
  }, [dispatch]);
  console.log("data",data);

  const [isOpen, setIsOpen] = useState(false);

  // Read Detail
  const [detail, setDetail] = useState([]);

  const openModal = (data) => {
    setDetail(data);
    setIsOpen(true);
  };
  const closeModal = () => {
    setIsOpen(false);
  };

  
  // Detele Task byId
  const HandleDeteleTask = (taskId) => {
    DELECT_TASK_BYID(taskId).then((res) => {
      console.log(taskId)
      dispatch(deleteTaskById(taskId))
    })
  }

  return (
    <div class=" pl-44 pr-44   pt-4 sm:ml-64">
       <h2 className="mb-2 pt-8 pb-8 text-5xl tracking-tight text-gray-900 dark:text-white">
        Categories / {propsData.categoryName}
      </h2>

      <div className="grid grid-cols-2 md:grid-cols-3 gap-4">
        

        {data?.map((data) => (
        //   <div
        //     key={item.taskId}
        //     className="w-12/12 h-40  text-left max-w-sm p-8 pl-12 mb-8 ml-8 bg-white border  rounded-lg shadow select-none border-l-4 border-blue-400  font-medium hover:border-blue-500"
        //   >
            
              
            
        //       <h4 className="mb-2  text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
        //       {moment(item?.timestamp).format("ddd,MMM DD")}
        //       </h4>
              
        //       <h4 className="mb-2  text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
        //       {item.taskName}
        //       </h4>

        //       <p className="mb-2  text-2xl  tracking-tight text-gray-900 dark:text-white">
        //       {item.description }
        //       </p>



             
          
            
        //     </div>

        <div class="p-3  rounded " key={data.taskId}>
              <div class=" w-96 bg-[#73B7F5] cursor-pointer border border-gray-200 rounded-3xl shadow">
                {/* Open modal  */}
                <div onClick={() => openModal(data)}>
                  <div class="flex justify-end px-4 pt-4">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-6 w-6"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      onClick={() => HandleDeteleTask(data.taskId)}
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M6 18L18 6M6 6l12 12"
                      />
                    </svg>
                  </div>

                  <div  class="flex flex-col px-4 pb-4">
                    <h1 className="font-medium text-2xl line-clamp-1">
                      {new Date(data.timestamp).toLocaleString("en-US", {
                        weekday: "short",
                        month: "short",
                        day: "2-digit",
                      })}
                    </h1>
                    <h2 className="text-2xl line-clamp-1">{data.taskName}</h2>
                    <p className="line-clamp-1">{data.description}</p>
                  </div>
                  <div>
                    <button
                      type="button"
                      class="bg-white shadow-lg mt-4  focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                    >
                      {data.status}
                    </button>
                  </div>
                </div>
              </div>
            </div>
     
        ))}
      </div>

    </div>
  )
}

export default CategoryView