import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { updateCategory } from "../../services/CategoryService";
import { Button, Checkbox, Label, Modal, TextInput } from "flowbite-react";

const CategoryUpdatePopUp = (data) => {
  const dispatch = useDispatch();

  const [show,setShow]=useState(false)
  // category
  const [categoryName, setCategoryName] = useState({});

  // const [categoryName,setCategoryName] = useState()
  const updateCategoryHandler = (e) => {
    console.log(data.itemUpdate.categoryId)
    e.preventDefault();
    const body = {
      categoryName,
      idUpdate:data.itemUpdate.categoryId,
    };
    dispatch(updateCategory(body));
    setShow(false)
  };
  return (
    <React.Fragment>
  <Label onClick={()=>setShow(true)}>
    Edit
  </Label>
  <Modal
    show={show}
    size="sm"
    popup={true}
    onClose={()=>setShow(false)}
  >
    <Modal.Header className="p-6">
     Update Category
    </Modal.Header>
    <Modal.Body>
      <div className="space-y-6 px-6 pb-4 sm:pb-6 lg:px-8 xl:pb-8">
        
        <div>
          <div className="mb-2 block">
            <Label
              htmlFor="email"
              value="Your email"
            />
          </div>
          <TextInput
            id="email"
            onBlur={(e)=>setCategoryName(e.target.value)}
            placeholder={data.itemUpdate.categoryName}
            required={true}
          />
        </div>
       
        <div className="w-full">
          <Button onClick={updateCategoryHandler}  type='submit' className="w-full">
            Update
          </Button>
        </div>
       
      </div>
    </Modal.Body>
  </Modal>
</React.Fragment>
  );
};

export default CategoryUpdatePopUp;
