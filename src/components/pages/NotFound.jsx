import React from 'react'
import notfound1 from '../utils/imgs/notfound1.svg'

const NotFound = () => {
  return (
    <>
      {/* <img src={notfound1} alt="" />
      <h1>Page not found!</h1>
      <h2>ស្គាល់ផ្លូវអត់?</h2> */}
      <div className='flex justify-center'>
        <img
          src={require("../../components/images/not-found.png")}
          className="w-[90%] h-[90%]"
          alt=""
        />
      </div>
    </>
  );
}

export default NotFound
