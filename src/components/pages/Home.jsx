import React from 'react'
import { Logout } from '../redux/features/logout/Logout'

const Home = () => {
  return (
    <>
      <div>Home</div>
      <Logout />
    </>
  )
}

export default Home
