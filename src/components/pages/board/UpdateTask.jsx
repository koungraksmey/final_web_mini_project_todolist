import React from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useLocation } from "react-router-dom";
import { updateTaskById } from "../../redux/features/Tasks/taskSlice";
import { UPDATE_TASK_BYID } from "../../services/TaskService";

const UpdateTask = () => {
  const { state } = useLocation();
  const taskId = state.taskId;
  const [title, setTitle] = useState("");
  const [status, setStatus] = useState("");
  const [description, setDescription] = useState("");
  console.log(description);
  const [taskName, settaskName] = useState("");
  const [date, setDate] = useState("");

  console.log(title);
  
  //   const allCates = useSelector((state) => state?.category?.category?.payload);
  //   const dispatch = useDispatch();



//   let tasks = {
//     taskName,
//     categoryId,
//     status,
//     description,
//     date,
//   };
  // Update by id
  //   const HandleUpdateTask = (taskId) => {
  //     UPDATE_TASK_BYID(taskId).then((res) => {
  //       console.log(taskId);
  //     //   dispatch(updateTaskById(taskId));
  //     });
  //   };

  return (
    <div>
      <div>
        <div className="px-40 pt-8 sm:ml-64 h-screen">
          <div>
            <h1 className="text-4xl my-16">Update Task</h1>
          </div>
          <form>
            <div className="flex w-full">
              <div className="w-full">
                <div className="grid gap-6 mb-6 pr-8 md:grid-cols-3">
                  <div>
                    <label
                      for="last_name"
                      className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                    >
                      Date
                    </label>
                    <input
                      type="datetime-local"
                      className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      required
                      // defaultValue=""
                      //   onChange={handleDateChange}
                    />
                  </div>

                  <div>
                    <label
                      for="last_name"
                      className="block mb-2 text-sm font-medium text-gray-900 "
                    >
                      Category
                    </label>
                    <div class="relative">
                      <select
                        class="block appearance-none w-full border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white "
                        id="grid-state"
                        defaultValue=""
                        // onChange={handleCategoryStateIdChange}
                      >
                        <option disabled selected>
                          select Category
                        </option>
                        {/* {allCates?.map((c) => (
                          <option value={c?.categoryId}>
                            {" "}
                            {c?.categoryName}
                          </option>
                        ))} */}
                      </select>
                      <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700"></div>
                    </div>
                  </div>

                  <div>
                    <label
                      for="last_name"
                      className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                    >
                      Status
                    </label>

                    <div class="relative">
                      <select
                        class="block appearance-none w-full  border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white "
                        id="grid-state"
                        defaultValue=""
                        // onChange={handleStatusChange}
                      >
                        <option disabled selected>
                          {" "}
                          select Status{" "}
                        </option>
                        <option value="is_complete">Done</option>
                        <option value="is_in_process">Process</option>
                        <option value="is_in_review">Review</option>
                        <option value="is_cancelled">Not yet</option>
                      </select>
                      <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700"></div>
                    </div>
                  </div>
                </div>

                <div className="mb-6 pr-7">
                  <label
                    for="text"
                    className="mb-2  text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Title
                  </label>
                  <input
                    type="text"
                    id="text"
                    name="title"
                    className="bg-gray-50 w-full border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Title"
                    required
                    onChange={(e) => setTitle(e.target.value)}
                    // value={taskName}
                    //   onChange={(e) => settaskName(e.target.value)}
                    // onChange={handleTittleChange}
                  />
                </div>
              </div>
              <div>
                <img
                  src={require("../../images/to-do-list.png")}
                  className="w- h-52"
                  alt=""
                />
              </div>
            </div>

            <label
              for="message"
              class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
            >
              Discription
            </label>
            <textarea
              name="description"
              id="message"
              rows="4"
              class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="This homework is about rested routes."
              //   onChange={handleDescriptionChange}
              onChange={(e) => setDescription(e.target.value)}
            ></textarea>
            <div className="mt-5">
              <button
                to=""
                type="button"
                class="focus:outline-none text-white bg-yellow-400 hover:bg-yellow-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:focus:ring-yellow-900"
              >
                <Link to="/home/board">Cancel</Link>
              </button>

              <button
                // onClick={HandleUpdateTask}
                type="button"
                class="focus:outline-none text-white bg-purple-700 hover:bg-purple-800 focus:ring-4 focus:ring-purple-300 font-medium rounded-lg text-sm px-5 py-2.5 mb-2 dark:bg-purple-600 dark:hover:bg-purple-700 dark:focus:ring-purple-900"
              >
                Update
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default UpdateTask;
