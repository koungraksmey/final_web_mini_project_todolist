import React, { useEffect, useState } from 'react'
import { DELECT_TASK_BYID, GET_ALL_TASK } from '../../services/TaskService'
import {
  deleteTaskById,
  getAllTaskSlice,
} from "../../redux/features/Tasks/taskSlice";
import { useDispatch, useSelector } from "react-redux";
import { Link, NavLink, useParams } from "react-router-dom";
import { Modal, Button } from "flowbite-react";
const BoardComponent = () => {
  const [isOpen, setIsOpen] = useState(false);

  // Read Detail
  const [detail, setDetail] = useState([]);

  const openModal = (data) => {
    setDetail(data);
    setIsOpen(true);
  };
  const closeModal = () => {
    setIsOpen(false);
  };

  const dispatch = useDispatch();
  const states = useSelector((state) => state?.task?.allTask);
  console.log("data from api", states);

  // Get All  Task
  useEffect(() => {
    console.log('my styate' + states)
    GET_ALL_TASK()
      .then((response) => {
        console.log(' Get Task All', response?.data?.payload)
        dispatch(getAllTaskSlice(response?.data?.payload))
      })
      .catch((err) => console.log("Errors : " + err));
  }, []);

  // Detele Task byId
  const HandleDeteleTask = (taskId) => {
    DELECT_TASK_BYID(taskId).then((res) => {
      console.log(taskId)
      dispatch(deleteTaskById(taskId))
    })
  }

  return (
    <div>
      <div className="pl-40 pt-8 sm:ml-64 text-black">
        <div className="flex  justify-around py-20 p-3 m-2">
          <h1 className="text-5xl font-extrabold text-transparent bg-clip-text bg-gradient-to-br from-[#2192FF] to-[#1C6DD0] ">
            All your board
          </h1>
          <div className=" float-right">
            <Link
              to="/home/addNewTask"
              class="inline-flex items-center justify-center w-full px-6 py-3 mb-2 text-lg text-white bg-green-500 rounded-md hover:bg-green-400 sm:w-auto sm:mb-0"
              data-primary="green-400"
              data-rounded="rounded-2xl"
              data-primary-reset="{}"
            >
              Add New Card
            </Link>
          </div>
        </div>
      </div>
      <div className="grid grid-cols-3 gap-1 pl-40 sm:ml-40">
        {/* task card */}
        {states?.map((data, index) => (
          <div class="  ">
            {/* <Button onClick={openModal}>Toggle modal</Button> */}
            <div class="p-3  rounded ">
              <div class=" w-96 bg-[#73B7F5] cursor-pointer border border-gray-200 rounded-3xl shadow">
                {/* Open modal  */}
                <div onClick={() => openModal(data)}>
                  <div class="flex justify-end px-4 pt-4">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-6 w-6"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      onClick={() => HandleDeteleTask(data.taskId)}
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M6 18L18 6M6 6l12 12"
                      />
                    </svg>
                  </div>

                  <div key={index} class="flex flex-col px-4 pb-4">
                    <h1 className="font-medium text-2xl line-clamp-1">
                      {new Date(data.timestamp).toLocaleString("en-US", {
                        weekday: "short",
                        month: "short",
                        day: "2-digit",
                      })}
                    </h1>
                    <h2 className="text-2xl line-clamp-1">{data.taskName}</h2>
                    <p className="line-clamp-1">{data.description}</p>
                  </div>
                  <div>
                    <button
                      type="button"
                      class="bg-white shadow-lg mt-4  focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                    >
                      {data.status}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
        <div className=" sm:ml-64">
          {/* pop up details */}
          <React.Fragment>
            <Modal
              className="w-80 backdrop-contrast-50"
              show={isOpen}
              onClose={closeModal}
            >
              <Modal.Header>{detail.taskName}</Modal.Header>
              <Modal.Body>
                <div className="space-y-6">
                  <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
                    {detail.description}
                  </p>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button onClick={openModal}> close</Button>
                <Button color="gray" onClick={closeModal}>
                  <Link state={detail} to="/home/updateTask">
                    Update
                  </Link>
                </Button>
              </Modal.Footer>
            </Modal>
          </React.Fragment>
        </div>
      </div>
    </div>
  )
}

export default BoardComponent
