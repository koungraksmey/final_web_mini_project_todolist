import React from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { Logout } from '../../redux/features/logout/Logout'

const SideBar = () => {
  const navigate = useNavigate()
  return (
    <div>
      <aside
        id="sidebar-multi-level-sidebar"
        class="fixed top-0 left-0 z-40 w-80 h-screen transition-transform -translate-x-full sm:translate-x-0"
        aria-label="Sidebar"
      >
        <div class="h-full px-3  flex justify-center text-xl py-4 overflow-y-auto bg-[#EFFAF9] dark:bg-gray-800">
          <ul class="space-y-2 mt-5 w-40 font-medium">
            <li className="relative gradient-bg rounded-l-lg w-[300px]">
              <button onClick={() => navigate('/')}>
                <div className=" w-full tracking-wider text-white rounded-xl px-8 py-1">
                  <h1 class=" font-bold text-xl ">Task Zone</h1>
                </div>
              </button>
            </li>
            <li>
              <Link
                to="/home/board"
                class="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-[#ffff] dark:hover:bg-gray-700"
              >
                <img
                  //   src={require("../images/book.png")}
                  src={require('../../images/book.png')}
                  className="w-5 h-5"
                  alt=""
                />
                <span class="ml-3">Boards</span>
              </Link>
            </li>
            <li>
              <Link
                to="addNewCategory"
                class="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-[#ffff] dark:hover:bg-gray-700"
              >
                <img
                  src={require('../../images/menu.png')}
                  className="w-5 h-5"
                  alt=""
                />
                <span class="flex-1 ml-3 whitespace-nowrap">Category</span>
              </Link>
            </li>
            <li className="ml-5 myDropdownbodyfont">
              {/* <h1>CSS Dropdown Menu</h1> */}
              <nav className="myDropDownNav">
                <label for="touch">
                  <span className="myStatus tracking-widest">Status</span>
                </label>
                <input type="checkbox" id="touch" />

                <ul class="slide">
                  <li className="myli">
                    <Link to="#" className="flex gap-2 items-center">
                      {' '}
                      <i class="fa-solid fa-check"></i> <span>Done</span>
                    </Link>
                  </li>
                  <li className="myli">
                    <Link to="#" className="flex gap-2 items-center">
                      {' '}
                      <i class="fa-solid fa-spinner"></i> <span>Procress</span>
                    </Link>
                  </li>
                  <li className="myli">
                    <Link to="#" className="flex gap-2 items-center">
                      {' '}
                      <i class="fa-solid fa-bookmark"></i> <span>Review</span>
                    </Link>
                  </li>
                  <li className="myli">
                    <Link to="#" className="flex gap-2 items-center">
                      <i class="fa-solid fa-note-sticky"></i>{' '}
                      <span>Not yet</span>
                    </Link>
                  </li>
                </ul>
              </nav>

              {/* original beblow 🎋 */}
              {/* <button
                type="button"
                class="flex items-center w-full p-2 text-gray-900 transition duration-75 rounded-lg group hover:bg-[#ffff] dark:text-white dark:hover:bg-gray-700"
                aria-controls="dropdown-example"
                data-collapse-toggle="dropdown-example"
              >
                <span
                  class="flex-1 ml-3 text-left whitespace-nowrap"
                  sidebar-toggle-item
                >
                  Status
                </span>
                <svg
                  sidebar-toggle-item
                  class="w-6 h-6"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fill-rule="evenodd"
                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                    clip-rule="evenodd"
                  ></path>
                </svg>
              </button> */}
              {/* <ul id="dropdown-example" class="hidden py-2 space-y-2">
                <li className="ml-5">
                  <Link
                    to="/"
                    class="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-[#ffff] dark:hover:bg-gray-700"
                  >
                    <img
                      src={require('../../images/checklist.png')}
                      className="w-5 h-5"
                      alt=""
                    />
                    <span class="flex-1 ml-3 whitespace-nowrap">Done</span>
                  </Link>
                </li>
                <li className="ml-5">
                  <Link
                    href="#"
                    class="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-[#ffff] dark:hover:bg-gray-700"
                  >
                    <img
                      src={require('../../images/deadline.png')}
                      className="w-5 h-5"
                      alt=""
                    />
                    <span class="flex-1 ml-3 whitespace-nowrap ">Process</span>
                  </Link>
                </li>
                <li className="ml-5">
                  <a
                    href="#"
                    class="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-[#ffff] dark:hover:bg-gray-700"
                  >
                    <img
                      src={require('../../images/review.png')}
                      className="w-5 h-5"
                      alt=""
                    />
                    <span class="flex-1 ml-3 whitespace-nowrap">Review</span>
                  </a>
                </li>
                <li className="ml-5">
                  <a
                    href="#"
                    class="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-[#ffff] dark:hover:bg-gray-700"
                  >
                    <img
                      src={require('../../images/clipboard.png')}
                      className="w-5 h-5"
                      alt=""
                    />
                    <span class="flex-1 ml-3 whitespace-nowrap ">Not yet</span>
                  </a>
                </li>
              </ul> */}
            </li>
            <li className="ml-5">
              <Logout />
            </li>
          </ul>
        </div>
      </aside>
    </div>
  )
}

export default SideBar
