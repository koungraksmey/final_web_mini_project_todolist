import { createAsyncThunk } from "@reduxjs/toolkit";
import { api } from "../utils/Api";
import { getTokenString } from "../utils/ProvideToken";


//Get All Category
export const fetchCategory = createAsyncThunk("category/fetchCategory", async () => {
    try {
        const token = JSON.parse(getTokenString())


        const response = await api.get("/categories/users?asc=false&desc=true&pages=1&size=10",
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                }
            }
        );
        // console.log(response.data.payload)
        return response.data.payload;
    } catch (err) {
        console.log(err);
    }
});

//Create Category
export const createCategory = createAsyncThunk("category/createCategory", async (body) => {
    try {
        const token = JSON.parse(getTokenString())
        const newObject = {
            categoryName: body.categoryName,
        }

        const response = await api.post("/categories", newObject,
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                }
            }
        );
        console.log(response)
        return response.data.payload;
    } catch (err) {
        console.log(err);
    }
});

// Update Category
export const updateCategory = createAsyncThunk("category/updateCategory", async (body) => {
    console.log( body)
    try {
        
        const token = JSON.parse(getTokenString())
        const newObject = {
            categoryName: body.categoryName,
        }

        const response = await api.put(`/categories/${body.idUpdate}`,
         newObject,
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                }
            }
        );
        return response.data.payload;
    } catch (err) {
        console.log(err);
    }
});

// delete Category
export const deleteCategory = createAsyncThunk("category/deleteCategory", async (body) => {
    console.log("body", body)
    try {
        
        const token = JSON.parse(getTokenString())

        const response = await api.delete(`/categories/${body.idDelete}/users`,
        
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                }
            }
        );
        console.log("res delelete",response);
        return body.idDelete
    } catch (err) {
        console.log(err);
    }
});

//Get All task by Category
export const getAllTaskByCategory = createAsyncThunk("category/getTaskByCategory", async (categoryId) => {
    try {
        const token = JSON.parse(getTokenString())


        const response = await api.get(`/tasks/category/${categoryId}`,
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                }
            }
        );
        // console.log(response.data.payload)
        return response.data.payload;
    } catch (err) {
        console.log(err);
    }
});


const categoryService = {
    fetchCategory,
    createCategory,
    updateCategory,
    deleteCategory
}

export default categoryService