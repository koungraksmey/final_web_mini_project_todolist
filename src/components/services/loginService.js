import axios from 'axios'

export async function REGISTER_SERVICE({ email, password }) {
  return await axios.post(
    'http://localhost:8888/api/v1/users/register',
    {
      name: email,
      email: email,
      password: password,
      roles: ['ROLE_USER'],
    },
    {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Credentials': 'true',
      },
    },
  )
}

export async function LOGIN_SERVICE({ email, password }) {
  const URL = 'http://localhost:8888/api/v1/auth/login'
  const body = {
    email,
    password,
  }

  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Allow-Credentials': 'true',
  }
  return await axios.post(URL, body, {
    headers: headers,
  })
}
