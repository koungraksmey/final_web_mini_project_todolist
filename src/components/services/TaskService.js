import { api } from '../utils/Api'
import { getTokenString } from '../utils/ProvideToken'

const token = JSON.parse(getTokenString())

export const GET_ALL_TASK = async () => {
  // console.log(token);
  let response = await api.get('/tasks', {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
  console.log('Response : ', response)
  return response
}
// Add Task
export const CREATE_TASK = async (task) => {
  console.log('hi', task)
  let response = await api.post(
    '/tasks/users',
    {
      taskName: task.taskName,
      categoryId: task.categoryId,
      description: task.description,
      date: task.date,
      status: task.status,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    },
  )
  console.log('Instat new task', response)
  return response
}
// delect
export const DELECT_TASK_BYID = async (taskId) => {
  let response = await api.delete(`/tasks/${taskId}/users`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
  console.log('Response : ', response)
  return response
}
// Update
export const UPDATE_TASK_BYID = async (task,taskId) => {
  console.log("ghuih",);
  // let response = await api.delete(
  //   `/tasks/${taskId}/users`,
  //   {
  //     taskName: task.taskName,
  //     categoryId: task.categoryId,
  //     description: task.description,
  //     date: task.date,
  //     status: task.status,
  //   },
  //   {
  //     headers: {
  //       Authorization: `Bearer ${token}`,
  //       "Content-Type": "application/json",
  //     },
  //   }
  // );
  // console.log("Response : ", response);
  // return response;
};
