import React from 'react'
import { Navigate, Outlet, useNavigate } from 'react-router-dom'
import { selectAlltoken } from '../redux/features/token/tokenSlice'
import { useSelector } from 'react-redux'
import SideBar from '../pages/sideBar/SideBar'

const ProtetedRoutesAndChild = ({ token, children }) => {
  console.log('proteted route: ' + token)
  if (!token) {
    return <Navigate to="/" />
  }
  return (
    <>
      {children}
      <Outlet />
    </>
  )
}

export default ProtetedRoutesAndChild
