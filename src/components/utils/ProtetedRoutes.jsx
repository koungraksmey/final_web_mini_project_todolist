import React from 'react'
import { Navigate, Outlet, useNavigate } from 'react-router-dom'
import { selectAlltoken } from '../redux/features/token/tokenSlice'
import { useSelector } from 'react-redux'

const ProtetedRoutes = ({ token }) => {
  console.log('proteted route: ' + token)
  if (!token) {
    return <Navigate to="/login" />
  }
  return <Outlet />
}

export default ProtetedRoutes
