import { useSelector } from 'react-redux'
import { selectAlltoken } from '../redux/features/token/tokenSlice'
import { useEffect, useState } from 'react'

export const useToken = () => {
  const { tempStoreToken } = useSelector(selectAlltoken)
  const storageToken = localStorage.getItem('token')
  const nextToken = Boolean(tempStoreToken) || Boolean(storageToken)
  return { nextToken }
}

export const getTokenString = () => {
  return localStorage.getItem('token')
}
