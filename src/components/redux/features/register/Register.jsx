import { Field, Form, Formik } from 'formik'
import React, { useEffect, useState } from 'react'
import { NavLink, useNavigate, Link } from 'react-router-dom'
import * as yup from 'yup'
import { REGISTER_SERVICE } from '../../../services/loginService'
import RegisterFailed from './RegisterFailed'
import { getTokenString } from '../../../utils/ProvideToken'

const Register = () => {
  const [registerErrorMessage, setRegisterErrorMessage] = useState(null)
  const token = getTokenString()
  const navegate = useNavigate()

  function handleRegister(userLogin) {
    REGISTER_SERVICE(userLogin)
      .then((res) => {
        if (res.status === 200 || res.data === 'Register successfully') {
          navegate('/home/board')
        }
        console.log(res)
      })
      .catch((err) => {
        const STATUS_CODE = err.response.status
        const DATA = err.response.data
        const errorDetails = { data: DATA, status: STATUS_CODE }
        setRegisterErrorMessage({ ...errorDetails })
      })
  }
  useEffect(() => {
    console.log(registerErrorMessage)
  }, [registerErrorMessage])

  const FormSchema = yup.object().shape({
    email: yup
      .string()
      .email('Must be a valid email')
      .matches(
        /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
        'Still not a valid email.',
      )
      .max(255)
      .required('Email is required'),
    password: yup
      .string()
      .required('Password is required')
      .min(8, 'Password must be 8 characters long')
      .matches(/[0-9]/, 'Password requires a number')
      .matches(/[a-z]/, 'Password requires a lowercase letter')
      .matches(/[A-Z]/, 'Password requires an uppercase letter')
      .matches(/[^\w]/, 'Password requires a symbol'),

    confirm: yup
      .string()
      .oneOf([yup.ref('pass'), null], 'Must match "password" field value'),
  })

  return (
    <>
      <header class="bg-[#4ECDC4] fixed top-0 right-0 left-0">
        <nav class="flex justify-between items-center py-4 px-6">
          <button
            onClick={() => navegate('/')}
            className="gradient-bg text-white rounded-xl px-2 py-1"
          >
            <h1 class=" font-bold text-xl ">Task Zone</h1>
          </button>
          <div>
            {/* <a
              href="#"
              class="bg-white text-indigo-500 py-2 px-4 rounded-full mr-4 hover:bg-indigo-500 hover:text-white transition duration-300 ease-in-out"
            >
              Login
            </a> */}
            <Link
              to={token == null ? '/login' : '/home/board'}
              class="bg-white text-indigo-500 py-2 px-4 rounded-full hover:bg-indigo-500 hover:text-white transition duration-300 ease-in-out"
            >
              Home
            </Link>
          </div>
        </nav>
      </header>
      <div className="smeyform gap-8 border max-md:pt-10 max-md:flex max-md:flex-col max-md:items-center max-md:justify-start h-screen md:px-4 flex flex-row w-full justify-center items-center ">
        <img
          src={require('../../../images/login.jpg')}
          className="w-4/12 pr-10  max-md:w-8/12"
          alt=""
        />
        <div className="smeyform border pl-10 ml-5 w-4/12 max-md:pr-0 md:pr-16 border-l-2 border-l-emerald-400 border-t-0 border-r-0 border-b-0">
          <div className="mb-5 pl-20">
            <h1 className="text-4xl font-bold mb-3 text-[#4ECDC4]">Sign Up</h1>
            {/* <p>Please enter your detail</p> */}
          </div>
          <Formik
            initialValues={{
              email: '',
              password: '',
            }}
            validationSchema={FormSchema}
            onSubmit={(values, { setSubmitting }) => {
              handleRegister({ email: values.email, password: values.password })
              console.log(values)
              setTimeout(() => {
                setSubmitting(false)
              }, 1500)
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
            }) => (
              <Form className="flex flex-col gap-8 pl-20">
                <div className="flex flex-col">
                  <label htmlFor="email" className="text-xl font-medium">
                    Email
                  </label>
                  <Field
                    className="rounded-xl my-1  focus:outline-none border-[#4ECDC4]"
                    type="email"
                    id="email"
                    name="email"
                  />
                  {errors.email && (
                    <p className="text-red-500">{errors.email}</p>
                  )}
                </div>
                <div className="flex flex-col">
                  <label htmlFor="password" className="text-xl font-medium">
                    Password
                  </label>
                  <Field
                    className="rounded-xl my-2  focus:outline-none border-[#4ECDC4]"
                    type="password"
                    id="password"
                    name="password"
                  />
                  {errors.password && (
                    <p className="text-red-500">{errors.password}</p>
                  )}
                </div>
                {registerErrorMessage && (
                  <RegisterFailed
                    registerErrorMessage={registerErrorMessage}
                    setRegisterErrorMessage={setRegisterErrorMessage}
                  />
                )}
                <button
                  className="text-lg w-full self-center bg-[#4ECDC4] py-0 rounded-lg flex flex-row items-center justify-center gap-2"
                  type="submit"
                  disabled={isSubmitting}
                >
                  {errors.email || errors.password ? (
                    <i class="fa-solid fa-lock text-white"></i>
                  ) : (
                    <i class="fa-solid fa-lock-open text-white"></i>
                  )}
                  <span className="px-3 py-2 text-white font-medium">
                    Sign Up
                  </span>
                </button>

                <div className="donthaveacc flex flex-row-2 gap-2 justify-center">
                  <h3 className="">Don't have an account?</h3>
                  <NavLink to="/login">
                    <span className="text-blue-600">Login</span>
                  </NavLink>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </>
  )
}

export default Register
