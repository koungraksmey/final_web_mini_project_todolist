import React from 'react'

const RegisterFailed = ({ registerErrorMessage, setRegisterErrorMessage }) => {
  const handleRemoveFor = () => {
    setRegisterErrorMessage('')
  }

  return (
    <>
      <div className="overlay">
        <div className=" sm:w-[350px] lg:w-[500px] pop rounded-lg slide-in-right w-[250px] bg-white min-h-[200px]">
          <div className="px-2 py-3">
            <div className="Opp! font-bold text-2xl ">Opp!</div>
            <div className="w-full my-3 h-1 bg-[#4ECDC4]"></div>
            <div className="detailErrorMessage font-bold">
              {registerErrorMessage?.data?.title + '!'}
            </div>
          </div>

          <span className=" absolute right-2 font-bold w-20 bottom-2 px-2 py-1 text-center rounded-xl bg-[#4ECDC4] cursor-pointer">
            <button
              type="submit"
              onClick={handleRemoveFor}
              className="text-white"
            >
              <i class="fa-solid fa-ban font-black"></i> Close
            </button>
          </span>
        </div>
      </div>
    </>
  )
}

export default RegisterFailed
