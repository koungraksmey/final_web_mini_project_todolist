import { createSlice } from "@reduxjs/toolkit";
import { DELECT_TASK_BYID } from "../../../services/TaskService";

const initialState = {
  allTask: [],
};
const taskSlice = createSlice({
  name: "task",
  initialState,
  reducers: {
    getAllTaskSlice: (state, action) => {
      console.log("actions : ", action);
      state.allTask = action.payload;
    },
    deleteTaskById: (state, action) => {
      console.log("action delete", action);
      state.allTask = state.allTask.filter(
        (task) => task.taskId !== action.payload
      );
    },

    updateTaskById: (state, action) => {
      console.log(action);
      state.allTask = action.payload;
    },
  },
});

export const { deleteTaskById } = taskSlice.actions;
export const { updateTaskById } = taskSlice.actions;
export const { getAllTaskSlice } = taskSlice.actions;
export default taskSlice.reducer;
