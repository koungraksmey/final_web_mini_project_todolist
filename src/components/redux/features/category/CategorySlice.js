import { createSlice } from "@reduxjs/toolkit";
import { createCategory, deleteCategory, fetchCategory, getAllTaskByCategory, updateCategory } from "../../../services/CategoryService";



// const initialState = {
//     loading : false,
//     category: [],
//     error: "",
// };

const categorySlice = createSlice({
    name: "category",
    initialState: {
        loading : false,
        categories: [],
        error: "",
    } ,
    reducers: {},
    extraReducers:(mapper)=>{
        //List Category
        mapper.addCase(fetchCategory.pending,(state)=>{
            state.loading = true;
        });
        mapper.addCase(fetchCategory.fulfilled,(state,action)=>{
            // console.log("action",action)
            state.loading = false;
            state.categories = action.payload;
        });
        mapper.addCase(fetchCategory.rejected,(state,action)=>{
            state.loading = false;
            state.error = action.error.message;
        });

        // Create Category
        mapper.addCase(createCategory.pending,(state)=>{
            state.loading = true;
        });
        mapper.addCase(createCategory.fulfilled,(state,action)=>{
            state.loading = false;
            state.categories.push(action.payload) ;
        });
        mapper.addCase(createCategory.rejected,(state,action)=>{
            state.loading = false;
            state.error = action.error.message;
        });

        // Update Category

        mapper.addCase(updateCategory.pending,(state)=>{
            state.loading = true;
        });
        mapper.addCase(updateCategory.fulfilled,(state,action)=>{
            state.loading = false;
            const indexUpdate = action.payload;
            const index = state.categories.findIndex((data)=>data.categoryId === indexUpdate.categoryId)
            console.log("index",index)           
             if(index !== -1){
                state.categories[index] = indexUpdate
            }
        });
        mapper.addCase(updateCategory.rejected,(state,action)=>{
            state.loading = false;
            state.error = action.error.message;
        });

        // Delete Category

        mapper.addCase(deleteCategory.pending,(state)=>{
            state.loading = true;
        });
        mapper.addCase(deleteCategory.fulfilled,(state,action)=>{
            
            state.loading = false;
            const indexDelete = action.payload
            console.log(indexDelete);
            
            state.categories = state.categories.filter((data)=> data.categoryId !== indexDelete)
            console.log("index",  state.categories)      

            //  if(index !== -1){
            //     state.categories[index] = indexDelete
            // }
        });
        mapper.addCase(deleteCategory.rejected,(state,action)=>{
            state.loading = false;
            state.error = action.error.message;
        });

        //List task by Category
        mapper.addCase(getAllTaskByCategory.pending,(state)=>{
            state.loading = true;
        });
        mapper.addCase(getAllTaskByCategory.fulfilled,(state,action)=>{
            // console.log("action",action)
            state.loading = false;
            state.categories = action.payload;
        });
        mapper.addCase(getAllTaskByCategory.rejected,(state,action)=>{
            state.loading = false;
            state.error = action.error.message;
        });

    }
    
});
export default categorySlice.reducer;