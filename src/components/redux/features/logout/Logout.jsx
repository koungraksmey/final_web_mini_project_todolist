import React from 'react'
import { useNavigate } from 'react-router-dom'

export const Logout = () => {
  const navigate = useNavigate()
  const handleLogout = () => {
    localStorage.removeItem('token')
    navigate('/login')
  }
  return (
    <div>
      <button onClick={handleLogout}>
        <div className="flex items-center justify-center">
          <img
            src={require('../../../images/export.png')}
            className="w-5 h-5 mr-2"
            alt=""
          />
          <span>Logout</span>
        </div>
      </button>
    </div>
  )
}
