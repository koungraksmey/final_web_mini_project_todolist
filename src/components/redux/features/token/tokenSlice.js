import { createSlice } from '@reduxjs/toolkit'

const initializeState = {}

const tokenSlice = createSlice({
  name: 'token',
  initialState: initializeState,
  reducers: {
    addToken: {
      reducer: (state, actions) => {
        state.token = actions.payload.token
      },
      prepare: (token) => {
        return {
          payload: {
            token,
          },
        }
      },
    },
  },
})

export const selectAlltoken = (state) => state.token
export const { addToken } = tokenSlice.actions
export default tokenSlice.reducer
