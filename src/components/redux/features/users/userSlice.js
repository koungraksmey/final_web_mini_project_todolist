import { createSlice } from '@reduxjs/toolkit'

const initializeState = [
  // {
  //   userId: 1,
  //   email: 'david',
  //   username: 'david@gmail.com',
  //   token: 'raefsafdmkwop14op234rmsopdfw9-e8ru34-rqw234ir32r.;',
  // },
  { ...localStorage.getItem('user') },
]

const usersSlice = createSlice({
  name: 'users',
  initialState: initializeState,
  reducers: {
    addUser: {
      reducer: (state, actions) => {
        // state.push(actions.payload)
        state[0].userId = actions.payload.userId
        state[0].email = actions.payload.email
        state[0].username = actions.payload.username
        state[0].token = actions.payload.token
      },
      prepare: (userId, email, username, token) => {
        return {
          payload: {
            userId: userId,
            email,
            username,
            token,
          },
        }
      },
    },
  },
})

export const selectAllUser = (state) => state.users
export const { addUser } = usersSlice.actions
export default usersSlice.reducer
