import { configureStore } from "@reduxjs/toolkit";
import userReducer from "../features/users/userSlice";
import tokenReducer from "../features/token/tokenSlice";
import taskSlice from "../features/Tasks/taskSlice";
import CategorySlice from "../features/category/CategorySlice";

export const store = configureStore({
  reducer: {
    users: userReducer,
    token: tokenReducer,
    task: taskSlice,
    category:CategorySlice,
  },
});
