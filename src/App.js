import './App.css'
import { Route, Routes } from 'react-router-dom'
import Home from './components/pages/Home'
import Login from './components/redux/features/login/Login'
import ProtetedRoute from './components/utils/ProtetedRoute'
import { selectAllUser } from './components/redux/features/users/userSlice'
import { useSelector } from 'react-redux'
import { useEffect, useState } from 'react'
import { selectAlltoken } from './components/redux/features/token/tokenSlice'
import PrivateRoute from './components/utils/PrivateRoute'
import ProtetedRoutes from './components/utils/ProtetedRoutes'
import NotFound from './components/pages/NotFound'
import Root from './components/pages/Root'
import BoardComponent from './components/pages/board/BoardComponent'
import CategoryComponent from './components/pages/category/CategoryComponent'
import ProtetedRoutesAndChild from './components/utils/ProtetedRoutesAndChild'
import 'flowbite'

import { useToken } from './components/utils/ProvideToken'
import LoginFail from './components/redux/features/login/LoginFail'
import TaskInsert from './components/pages/board/TaskInsert'
import Register from './components/redux/features/register/Register'
import WelcomePage from './components/pages/WelcomePage'
import SideBar from './components/pages/sideBar/SideBar'
import CategoryView from './components/pages/category/CategoryView'
import UpdateTask from './components/pages/board/UpdateTask'
function App() {
  const { nextToken } = useToken()

  return (
    <>
      <div>
        <Routes>
          <Route path="login" element={<Login />} />
          <Route path="signup" element={<Register />} />
          <Route path="*" element={<NotFound />} />
          <Route path="/" element={<WelcomePage />} />
          <Route
            path="home"
            element={
              <ProtetedRoutesAndChild
                token={nextToken}
                children={<SideBar />}
              />
            }
          >
            <Route index element={<BoardComponent />} />
            <Route path="board" element={<BoardComponent />} />
            <Route path="addNewTask" element={<TaskInsert />} />
            <Route path='updateTask' element={<UpdateTask/>}/>
            <Route path="addNewCategory" element={<CategoryComponent />} />
            <Route  path="taskByCategory" element={<CategoryView />} />
          </Route>

          {/* TODO: Testing simple for protected many routes ​*/}
          <Route path="hometest" element={<ProtetedRoutes token={nextToken} />}>
            <Route path="test" element={<Home />} />
            <Route path="test2" element={<Home />} />
          </Route>
          <Route
            path="hometest"
            element={<ProtetedRoute token={nextToken} children={<Home />} />}
          />
        </Routes>
      </div>
    </>
  )
}

export default App
